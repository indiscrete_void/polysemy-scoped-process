module Polysemy.Resize
  ( Size,
    Resize,
    resize,
    resizeToIO,
    ps2s,
  )
where

import Control.Monad
import Data.Bifunctor
import Data.Int
import Data.Kind
import Polysemy
import System.Posix.Pty

type Size :: Type
type Size = (Int16, Int16)

type Resize :: Effect
data Resize m a where
  Resize :: Size -> Resize m ()

makeSem ''Resize

ps2s :: Size -> (Int, Int)
ps2s = join bimap fromIntegral

resizeToIO :: (Member (Embed IO) r) => Pty -> InterpreterFor Resize r
resizeToIO pty = interpret \case
  Resize size -> embed $ resizePty pty (ps2s size)
