module Polysemy.Process
  ( StreamKind (..),
    ProcessEffects,
    Process,
    exec,
    wait,
    scopedProcToIOFinal,
    execIO,
    ioShell,
    ioProc,
  )
where

import Control.Monad
import Data.ByteString (ByteString)
import Data.Int
import Data.Kind
import Data.Maybe
import GHC.Generics
import Polysemy
import Polysemy.Bundle
import Polysemy.Close
import Polysemy.Input
import Polysemy.Internal.Kind
import Polysemy.Resource
import Polysemy.Scoped
import Polysemy.ScopedBundle
import Polysemy.Tagged
import Polysemy.Trace
import Polysemy.Transport
import Polysemy.Wait
import System.IO
import System.Process
import Transport.Maybe
import Prelude hiding (read)

type StreamKind :: Type
data StreamKind = StandardStream | ErrorStream
  deriving stock (Generic, Show)

type ProcessEffects :: [Effect]
type ProcessEffects = ByteOutput ': Tagged 'StandardStream ByteInputWithEOF ': Tagged 'ErrorStream ByteInputWithEOF ': Wait ': Close ': '[]

type Process :: Effect
type Process = Bundle ProcessEffects

bundleProcEffects :: (Member Process r) => InterpretersFor ProcessEffects r
bundleProcEffects =
  sendBundle @Close @ProcessEffects
    . sendBundle @Wait @ProcessEffects
    . sendBundle @(Tagged 'ErrorStream ByteInputWithEOF) @ProcessEffects
    . sendBundle @(Tagged 'StandardStream ByteInputWithEOF) @ProcessEffects
    . sendBundle @ByteOutput @ProcessEffects

exec :: (Member (Scoped p Process) r) => p -> InterpretersFor ProcessEffects r
exec params = scoped @_ @Process params . bundleProcEffects . insertAt @5 @'[Process]

execIO :: (Member (Scoped p Process) r) => p -> InterpretersFor (Append (TransportEffects ByteString ByteString) '[Wait]) r
execIO params = exec params . subsume_ . tagged @'StandardStream @ByteInputWithEOF

ioShell :: String -> CreateProcess
ioShell cmd = (shell cmd) {std_in = CreatePipe, std_out = CreatePipe, std_err = Inherit}

ioProc :: String -> [String] -> CreateProcess
ioProc name args = (proc name args) {std_in = CreatePipe, std_out = CreatePipe, std_err = Inherit}

scopedProcToIOFinal :: (Member (Final IO) r, Member Trace r) => Int -> InterpreterFor (Scoped CreateProcess Process) r
scopedProcToIOFinal bufferSize = embedToFinal @IO . runScopedBundle (procParamsToIOFinal bufferSize) . raiseUnder

procParamsToIOFinal :: (Member (Final IO) r, Member Trace r) => Int -> CreateProcess -> InterpretersFor ProcessEffects r
procParamsToIOFinal bufferSize param sem = resourceToIOFinal $ bracket (openProc param) closeProc (raise . go)
  where
    openProc params = embedFinal $ createProcess params
    closeProc hs = embedFinal $ cleanupProcess hs
    go hs = embedToFinal @IO $ unmaybeHandles hs >>= flip (procToIO bufferSize) (insertAt @5 @'[Embed IO] sem)
    unmaybeHandles (mi, mo, me, ph) = do
      i <- unmaybeHandle @IO mi
      o <- unmaybeHandle @IO mo
      return (i, o, me, ph)

procToIO :: (Member (Embed IO) r, Member Trace r) => Int -> (Handle, Handle, Maybe Handle, ProcessHandle) -> InterpretersFor ProcessEffects r
procToIO bufferSize (i, o, e, ph) =
  closeToIO i
    . waitToIO ph
    . (maybeInputToIO bufferSize e . untag @'ErrorStream)
    . (inputToIO bufferSize o . untag @'StandardStream)
    . outputToIO i

maybeInputToIO :: (Member (Embed IO) r, Member Trace r) => Int -> Maybe Handle -> InterpreterFor ByteInputWithEOF r
maybeInputToIO bufferSize mh = interpret \case
  Input -> do
    h <- unmaybeHandle @IO mh
    inputToIO bufferSize h input

unmaybeHandle :: forall m r a. (MonadFail m, Member (Embed m) r) => Maybe a -> Sem r a
unmaybeHandle = embed @m . maybeFail "required process stream isn't piped"
